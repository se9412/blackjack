package BlackJack;

public class Dealer extends Participant {

	//得点が17未満ならカードを引く
	public void Decision(){
		//得点が17点未満の間ループする
		while(getScore() < 17){
			System.out.println("ディーラーの引いたカードは、" + Draw() + "です。");
			System.out.println("ディーラーの得点は、" + getScore() + "です。");
			//得点が21点を超えるとバースト
			if(getScore() > 21){
				System.out.println("ディーラーはバーストしました。");
			//得点が21点になるとブラックジャック
			} else if(getScore() == 21){
				System.out.println("ディーラーはブラックジャックです！");
			}
		}
	}
}
