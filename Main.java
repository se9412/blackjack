package BlackJack;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		Deck deck = new Deck();
		Player player = new Player();
		Dealer dealer = new Dealer();
		System.out.println("ブラックジャックスタート");
		System.out.println("あなたの引いたカードは、" + player.Draw() + "です。");
		System.out.println("あなたの引いたカードは、" + player.Draw() + "です。");
		if(player.getScore() == 21){
			System.out.println("ブラックジャックです！！");
		}
		System.out.println("現在のあなたの得点は、" + player.getScore() +"です。");
		System.out.println("\nディーラーの引いたカードは、" + dealer.Draw() + "です。");
		System.out.println("ディーラーの2枚目のカードはまだ公開されません。");
		System.out.println("ディーラーの得点は、" + dealer.getScore() + "です。");
		//プレイヤーがカードを引くかどうか選択
		player.YesOrNo();
		System.out.println("\nディーラーの2枚目のカードが公開されました。");
		System.out.println("2枚目のカードは、" + dealer.Draw() + "でした。");
		System.out.println("ディーラーの得点は、" + dealer.getScore() + "です。");
		//ディーラーがカードを引く
		dealer.Decision();
		System.out.println("\nあなたの得点：" + player.getScore());
		System.out.println("ディーラーの得点：" + dealer.getScore());
		//勝敗の判定
		//プレイヤーとディーラーがバーストしていない状態でプレイヤーの方が21点に近いとき、
		//またはディーラーだけがバーストしている時、プレイヤーの勝ち
		//負けは上記のプレイヤーとディーラーの立場が入れ替わっただけ
		//得点が同じ、またはどちらもバーストしているとき、引き分け
		if(((player.getScore() < 22 && dealer.getScore() < 22) && (21 - player.getScore()) < (21 - dealer.getScore())) || (player.getScore() < 22 && dealer.getScore() > 21)){
			System.out.println("\nあなたの勝ちです！！！");
		} else if(((dealer.getScore() < 22 && player.getScore() < 22) && (21 - dealer.getScore()) < (21 - player.getScore())) || (dealer.getScore() < 22 && player.getScore() > 21)){
			System.out.println("\nあなたの負けです………");
		} else if(player.getScore() == dealer.getScore() || (player.getScore() > 21 && dealer.getScore() > 21)){
			System.out.println("\n引き分けでした。");
		}
	}
}