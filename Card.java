package BlackJack;

public class Card {
	//カードの数字、値
	private int[] numbers = {1,2,3,4,5,6,7,8,9,10,11,12,13};
	//カードの記号
	private String[] marks = {"ハート", "ダイヤ", "スペード", "クローバー"};


	//numbersの値の1,11,12,13を指定したアルファベットに変更して
	//戻り値のcardNumbers配列に格納
	public String[] NumbersString(){
		String[] cardNumbers = new String[13];
		for(int i = 0; i < numbers.length; i++){
			switch(numbers[i]){
				case 1:
					cardNumbers[i] = "A";
					break;
				case 11:
					cardNumbers[i] = "J";
					break;
				case 12:
					cardNumbers[i] = "Q";
					break;
				case 13:
					cardNumbers[i] = "K";
					break;
				default:
					cardNumbers[i] = Integer.toString(numbers[i]);
					break;
			}
		}
		return cardNumbers;
	}

	//numbersの1,11,12,13を設定したい得点に変更して
	//戻り値のcardPoint配列に格納
	public int[] CardPoint(){
		int[] cardPoint = new int[13];
		for(int i = 0; i < numbers.length; i++){
			switch(numbers[i]){
				case 1:
					cardPoint[i] = 11;
					break;
				case 11:
					cardPoint[i] = 10;
					break;
				case 12:
					cardPoint[i] = 10;
					break;
				case 13:
					cardPoint[i] = 10;
					break;
				default:
					cardPoint[i] = numbers[i];
					break;
			}
		}
		return cardPoint;
	}


	//getter/setter
	public int[] getNumbers() {
		return numbers;
	}
	public void setNumbers(int[] numbers) {
		this.numbers = numbers;
	}
	public String[] getMarks() {
		return marks;
	}
	public void setMarks(String[] marks) {
		this.marks = marks;
	}

}
