package BlackJack;

import java.util.LinkedList;

//PlayerとDealerクラスの親クラス
public class Participant {
	//得点を格納する
	private int score = 0;
	private LinkedList<String> hand = new LinkedList<String>();
	private LinkedList<Integer> handPoint = new LinkedList<Integer>();


	//カードを引くメソッド
	public String Draw(){
		//deckの0番目のカードを引く
		//同じカードを引くことを防ぐために、deckの0番目のカードを削除
		//手札(hand)に引いたカードを追加
		String draw = Deck.deck.get(0);
		Deck.deck.remove(0);
		hand.add(draw);

		//pointListの0番目の得点を取得
		//取得したpointListの0番目の得点を削除
		//持ち点(handPoint)に取得した得点を追加
		int cardPoint = Deck.pointList.get(0);
		Deck.pointList.remove(0);
		handPoint.add(cardPoint);

		//scoreにhandPoint配列の合計を代入
		score = 0;
		for(int i = 0; i < handPoint.size(); i++){
			score += handPoint.get(i);
		}

		//Aが手札にあるとき、かつ、得点が21点を超えるとき、Aは1点扱いになる
		if(score > 21 && handPoint.indexOf(11) >= 0){
			//11点として格納されている得点を1点に書き換える
			//11点から1点に書き換えた為、その差分をscoreから引く
			handPoint.set(handPoint.indexOf(11), 1);
			score -= 10;
		}
		return draw;
	}


	//getter/setter
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public LinkedList<String> getHand() {
		return hand;
	}


	public void setHand(LinkedList<String> hand) {
		this.hand = hand;
	}


	public LinkedList<Integer> getHandPoint() {
		return handPoint;
	}


	public void setHandPoint(LinkedList<Integer> handPoint) {
		this.handPoint = handPoint;
	}


}
