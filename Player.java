package BlackJack;

import java.util.Scanner;

public class Player extends Participant {

	//カードを引くのか決める
	public void YesOrNo(){
		//得点が21点未満の間ループする
		while(getScore() < 21){
			//カードを引くかどうか
			System.out.println("カードを引きますか？(ヒット:y　スタンド:n)");
			String answer = new Scanner(System.in).nextLine();
			//yと入力されるとカードを引く
			if(answer.equals("y")){
				System.out.println("あなたの引いたカードは、" + Draw() + "です。");
				System.out.println("現在のあなたの得点は、" + getScore() +"です。");
				//21点を超えるとバースト
				if(getScore() > 21){
					System.out.println("バーストしてしまいました。");
				//21点になるとブラックジャック
				} else if(getScore() == 21){
					System.out.println("ブラックジャックです！！");
				}
			//y以外の入力があったとき
			} else {
				//得点が表示されてループ終了
				System.out.println(getScore() +"点でディーラーと勝負です。");
				break;
			}
		}
	}

}
