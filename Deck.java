package BlackJack;

import java.util.LinkedList;

public class Deck {
	//山札(deck)と各カードの得点(pointList)
	static LinkedList<String> deck = new LinkedList<String>();
	static LinkedList<Integer> pointList = new LinkedList<Integer>();
	//Cardのインスタンス
	Card card = new Card();


	//山札の作成とシャッフルするコンストラクタ
	//"〇〇のカード"という形のカードを山札(deck)に追加
	public Deck(){
		for(int i = 0; i < card.getMarks().length; i++){
			for(int j = 0; j < card.getNumbers().length; j++){
				deck.add(card.getMarks()[i] + "の" + card.NumbersString()[j]);
				pointList.add(card.CardPoint()[j]);
			}
		}
		//生成した山札(deck)とpointListをシャッフルする(FisherYatesアルゴリズム)
		for(int k = deck.size() - 1; k > 0; k--){
			int l = (int) Math.floor(Math.random() * (k + 1));
			String tmp = deck.get(k);
			deck.set(k, deck.get(l));
			deck.set(l, tmp);
			int pointTmp = pointList.get(k);
			pointList.set(k, pointList.get(l));
			pointList.set(l, pointTmp);
		}
	}


}
